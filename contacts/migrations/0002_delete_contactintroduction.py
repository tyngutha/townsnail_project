# Generated by Django 3.0.4 on 2020-04-20 19:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ContactIntroduction',
        ),
    ]
